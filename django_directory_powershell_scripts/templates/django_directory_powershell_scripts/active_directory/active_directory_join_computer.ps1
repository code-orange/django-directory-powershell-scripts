# Active Directory Join Computer
#Requires -RunAsAdministrator

Rename-Computer "{{ computer_name|upper }}" -Force

$join_cred = New-Object pscredential -ArgumentList ([pscustomobject]@{
    UserName = $null
    Password = (ConvertTo-SecureString -String "{{ computer_init_password }}" -AsPlainText -Force)[0]
})

Add-Computer -Domain "intra.srvfarm.net" -Options UnsecuredJoin,PasswordPass,JoinWithNewName -Credential $join_cred
Add-LocalGroupMember -Member {{ company_super_admin_sid }} -SID S-1-5-32-544
Add-LocalGroupMember -Member {{ company_admin_group_sid }} -SID S-1-5-32-544
Restart-Computer -Force
