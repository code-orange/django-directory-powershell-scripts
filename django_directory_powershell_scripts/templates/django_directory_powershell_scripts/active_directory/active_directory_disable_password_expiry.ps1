# Active Directory Disable Password Expiry
Get-ADUser -Filter * -SearchBase "{{ hosting_base_dn }}" | Set-ADUser -PasswordNeverExpires $True
