# Active Directory Disable Password Change
Get-ADUser -Filter * -SearchBase "{{ hosting_base_dn }}" | Set-ADUser -CannotChangePassword $True
