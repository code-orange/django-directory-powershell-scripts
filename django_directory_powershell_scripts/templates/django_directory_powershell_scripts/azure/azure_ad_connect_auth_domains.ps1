$ErrorActionPreference = "Stop"

$ms_tenant_domain = "{{ ms_tenant_domain }}"
$ms_tenant_id = "{{ ms_tenant_id }}"
$tenant_domains = {{ tenant_domains|safe }}

$sso_brand = "SRVFARM"
$sso_loginurl = "https://sso.srvfarm.net/saml/singleSignOn"
$sso_logout_url = "https://sso.srvfarm.net/?logout=1"
$sso_metadata_url = "https://sso.srvfarm.net/saml/metadata"

# Read Certificate
$sso_metadata = New-Object System.Xml.XmlDocument
$sso_metadata.Load($sso_metadata_url)

$sso_cert = $sso_metadata.EntityDescriptor.IDPSSODescriptor.KeyDescriptor.KeyInfo.X509Data.X509Certificate[0]

Write-Host "Setting default Domain to $ms_tenant_domain."
Set-MsolDomain -TenantId $ms_tenant_id -Name $ms_tenant_domain -IsDefault:$true

foreach ($tenant_domain in $tenant_domains) {
    Write-Host "Register $tenant_domain in $ms_tenant_domain."
    New-MsolDomain -TenantId $ms_tenant_id -Name $tenant_domain -ErrorAction SilentlyContinue

    if (Get-MsolDomain -TenantId $ms_tenant_id -DomainName $tenant_domain -ErrorAction SilentlyContinue) {
        # Verify Domain
        $tenant_domain_verify = Get-MsolDomainVerificationDns -TenantId $ms_tenant_id -DomainName $tenant_domain -Mode DnsTxtRecord
        $tenant_txt_record = $tenant_domain_verify.Text
        Write-Host "TXT-Record for $tenant_domain is: $tenant_txt_record"

        # Confirm Domain
        $domain = Get-MsolDomain -TenantId $ms_tenant_id -DomainName $tenant_domain
        if ($domain.Status -eq "Unverified") {
            Set-MsolDomainAuthentication -TenantId $ms_tenant_id -DomainName $tenant_domain -Authentication managed -ErrorAction SilentlyContinue | Out-Null
            Confirm-MsolDomain -TenantId $ms_tenant_id -DomainName $tenant_domain -ErrorAction SilentlyContinue
        }

        # Setup Federation
        Write-Host "Setting up federated login for $tenant_domain."
        $local_sso_loginurl = $sso_loginurl + "?domain=$tenant_domain"
        $local_metadata_url = $sso_metadata_url + "?domain=$tenant_domain"

        $local_sso_brand = $sso_brand + ' ' + $tenant_domain

        Set-MsolDomainAuthentication -TenantId $ms_tenant_id -DomainName $tenant_domain -Authentication managed -ErrorAction SilentlyContinue | Out-Null
        Set-MsolDomainAuthentication -TenantId $ms_tenant_id -DomainName $tenant_domain -FederationBrandName $local_sso_brand -Authentication Federated -PassiveLogOnUri $local_sso_loginurl -SigningCertificate $sso_cert -IssuerUri $local_metadata_url -LogOffUri $sso_logout_url -PreferredAuthenticationProtocol SAMLP -SupportsMfa $true -ErrorAction SilentlyContinue | Out-Null
    }
}
