$ErrorActionPreference = "Stop"

$ms_tenant_domain = "{{ ms_tenant_domain }}"
$ms_tenant_id = "{{ ms_tenant_id }}"

{% for tenant_user in tenant_users %}
Set-MsolUser -TenantId $ms_tenant_id -ObjectId {{ tenant_user.object_id }} -ImmutableId {{ tenant_user.immutable_id }} -ErrorAction SilentlyContinue
Set-MsolUserPrincipalName -TenantId $ms_tenant_id -ObjectId {{ tenant_user.object_id }} -NewUserPrincipalName {{ tenant_user.upn }} -ImmutableId {{ tenant_user.immutable_id }} -ErrorAction SilentlyContinue
{% endfor %}
