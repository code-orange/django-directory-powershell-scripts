import tempfile

from django.core.management.base import BaseCommand

from django_directory_api.django_directory_api.func import (
    generate_azure_ad_connect_auth_domains,
)
from django_directory_models.django_directory_models.models import (
    DirectoryTenantSettings,
)


class Command(BaseCommand):
    def handle(self, *args, **options):
        # Generate temp folder
        temp_folder = tempfile.mkdtemp()
        print("Created temporary directory", temp_folder)

        for tenant in DirectoryTenantSettings.objects.all():
            filename = (
                temp_folder
                + "/"
                + tenant.customer.org_tag
                + "_azure_ad_connect_auth_domains.ps1"
            )
            with open(filename, "w") as ps1script:
                ps1script.write(generate_azure_ad_connect_auth_domains(tenant.customer))
